﻿#!/bin/bash

date=`date +%d-%m-%Y`

function GetCredentials
{
    if [ -f cookie.txt ] ; then rm cookie.txt
	fi
    curl -siL 'https://portalpacjenta.luxmed.pl/PatientPortal/Account/LogIn' --data 'Login='$login'&Password='$password'' -c cookie.txt | grep 'name="__RequestVerificationToken\|ASP.NET_SessionId\|__RequestVerificationToken_' | cut -d ";" -f1 > cookie.txt
    cookie=$(cat cookie.txt | cut -d " " -f2 | grep -iv action | sed -e 'H;${x;s/\n/;/g;s/^,//;p;};d' | cut -d ";" -f2-);
    token=$(cat cookie.txt | grep -iPoa "value=\".*" | cut -d "\"" -f16);
}

function GetRequest
{   
    printf "*Pozycje wymagane!!!\n\n"
    printf "*Wybierz ID miasta\n";
    curl -s 'https://portalpacjenta.luxmed.pl/PatientPortal/'  -H 'Cookie: '$cookie'' | grep -iPoa 'option class=\"\" value=.*?<' | cut -d "\"" -f4- | sed 's/\">/ /g' | tr -d "<" | sed -e 's/&#243;/o/g'
    read -r cityId
    while [[ ! $cityId =~ ^[0-9]+$ ]]; do
    	    echo "Podaj poprawne dane!";
    	    read -r cityId
    done
    reset

    printf "Wybierz ID Placowki. Jesli brak, szukac bedzie po calym miescie\n\n";
    curl -s 'https://portalpacjenta.luxmed.pl/PatientPortal/Home/GetFilter/'  -H 'Cookie: '$cookie'' --data 'cityId='$cityId'&postId=&serviceId=&dateFrom='$date''| tr "\n" " " | grep -iPoa '<select id=\"ClinicId\".*?</select>' | grep -iPoa 'option class=\"\" value=.*?<' | cut -d "\"" -f4- | sed 's/\">/ /g' | tr -d "<" | sed -e 's/&#243;/o/g'
    read  -r clinicId
    reset
    
    printf "*Wybierz ID Uslugi.\n\n";
    curl -s 'https://portalpacjenta.luxmed.pl/PatientPortal/Home/GetFilter/'  -H 'Cookie: '$cookie'' --data 'cityId='$cityId'&postId='$clinicId'&serviceId=&dateFrom='$date''| tr "\n" " " | grep -iPoa '<select id=\"ServiceId\".*?</select>' | grep -iPoa 'option class=\"\" value=.*?<' | cut -d "\"" -f4- | sed 's/\">/ /g' | tr -d "<" | sed -e 's/&#243;/o/g'
    read  -r serviceId
    while [[ ! $serviceId =~ ^[0-9]+$ ]]; do
    	    echo "Podaj poprawne dane!";
    	    read -r serviceId
    done
    reset
    
    printf "Wybierz ID Lekarza. Jesli brak, szukac bedzie dla wszystkich\n\n";
    curl -s 'https://portalpacjenta.luxmed.pl/PatientPortal/Home/GetFilter/'  -H 'Cookie: '$cookie'' --data 'cityId='$cityId'&postId='$clinicId'&serviceId='$serviceId'&dateFrom='$date''| tr "\n" " " | grep -iPoa '<select id=\"DoctorId\".*?</select>' | grep -iPoa 'option class=\"\" value=.*?<' | cut -d "\"" -f4- | sed 's/\">/ /g' | tr -d "<" | sed -e 's/&#211;/O/g'
    read  -r doctorId
    reset
    
    printf "*Podaj zakres dni, w których ma szukac wizyt. Od dzis do?\nWymagana skladnia dd-mm-yyyy\n";
    read -r toDate
    while [[ !  $toDate =~ ^[0-9]{2}-[0-9]{2}-[0-9]{4}$ ]]; do
        echo "Podaj poprawna wartosc";
        read -r toDate
    done
    reset
    
    printf "*Podaj ID klienta.\n\n";
    read -r payerId
    while [[ ! $payerId =~ ^[0-9]+$ ]]; do
    	    echo "Podaj poprawne dane!";
    	    read -r payerId
    done
    reset
    
    day=$(echo $toDate | cut -d "-" -f1);
    month=$(echo $toDate | cut -d "-" -f2);
    year=$(echo $toDate | cut -d "-" -f3);
}


function main
{
    echo "Szukanie w toku..."
    while true; do
        curl -s 'https://portalpacjenta.luxmed.pl/PatientPortal/Reservations/Reservation/Find' -H 'Cookie: '$cookie'' --data 'PayerId='$payerId'&SearchFirstFree=True&__RequestVerificationToken='$token'&TimeOption=Any&CityId='$cityId'&ClinicId='$clinicId'&ServiceId='$serviceId'&DoctorId='$doctorId'&FromDate='$date'&ToDate='$toDate''  > result.html
        if [[ $(cat result.html | grep -i unauthorized) != "" ]] ; then
            GetCredentials
            continue
        fi
        
        result=$(cat result.html | tr -d "\n" | grep -iPoa '/PatientPortal/Reservations/Reservation/ReservationConfirmation.*?"' | grep 'DoctorId='$doctorId'' | grep -iPoa 'date=.*?;' | cut -d "&" -f1 | cut -d = -f2 |sed -e 's/%2F/-/g' | sed -e 's/%20/_/g' | sed -e 's/%3A/:/g'  | sort | uniq)
        for line in $result; do
	    line=$(echo $line | sed -e 's/_/ /g')
	    dayFound=$(echo $line | cut -d - -f2)
	    monthFound=$(echo $line | cut -d - -f1)
	    yearFound=$(echo $line | cut -d - -f3 | awk '{print $1;}')
            if [[ $dayFound -le $day ]] && [[ $monthFound -le $month ]] && [[ $yearFound -le $year ]]  ; then
	        if [[ $(cat found.txt | grep "$line" ) == "" ]] ; then
	            echo $line >> found.txt;
	          #  php sendMail.php $line;
	        fi
            fi
        done
        rm result.html
        sleep 60
    done
}

if [[ $# -ne 2 ]] ; then echo "Uzycie: ./luxmed.sh login password" ; exit
else
  login=$1
  password=$2
  GetCredentials
  if [ -z "$token" ]; then
    echo "Zle dane do logowania"
    exit
  fi
  GetRequest
  main
fi
